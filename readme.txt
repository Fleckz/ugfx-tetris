A demo tetris game using µGFX ( a.k.a. uGFX )

**NOTE: Please, edit Makefiles, change GFXLIB  path accordingly.**

[Screenshot](https://bitbucket.org/Fleckz/ugfx-tetris/src/463cdb05b2835d0c5657d33b3210403aed45159b/tetris_screenshot_small.png?at=master)

This project is discontinued and most likely will not get updated anymore as I don't have interest in uGFX anymore! Feel free to fork/change/update as you wish! :)