#ifndef _TETRIS_H_
#define _TETRIS_H_

#define TETRIS_CELL_WIDTH       17
#define TETRIS_CELL_HEIGHT      17
#define TETRIS_FIELD_WIDTH      10
#define TETRIS_FIELD_HEIGHT     17
#define TETRIS_SHAPE_COUNT      7

// Size of 7-segment styled numbers
#define SEVEN_SEG_SIZE          0.8

void tetrisInit(void);
void tetrisStart(void);

#endif  /* _TETRIS_H_ */
