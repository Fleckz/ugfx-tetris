#include "gfx.h"
#include "tetris.h"

int main(void)
{
	gfxInit();

	tetrisInit();

	while (TRUE) {
		// Start a new game
		// Will return when game is over
		tetrisStart();

		gfxSleepMilliseconds(10000);
	}
}
